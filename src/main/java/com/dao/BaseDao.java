/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 * @author vikas
 */
public abstract class BaseDao {

    protected Session session = null;
    protected  Transaction transaction = null;

    public BaseDao() {
    }

    protected Session getSession() {
        if (session == null) {
            session = getSessionFactory().openSession();
        }
        return session;
    }

    protected abstract SessionFactory getSessionFactory();

    protected Transaction getTransaction() {

        if (transaction == null) {
            transaction = getSession().getTransaction();
        }
        return transaction;
    }

    protected void beginTransaction() {
        getTransaction().begin();

    }

    protected void endTransaction() {
        getSession().flush();
        if (transaction != null) {
            transaction.commit();
            transaction = null;
            session = null;
        }
    }
}
