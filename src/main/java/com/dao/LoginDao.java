/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dao;

import com.entity.LoginEntity;
import java.util.List;

/**
 *
 * @author vikas
 */
public interface LoginDao {

    public boolean authentication(String userName, String password);

    public List<LoginEntity> getUserList();

    public void addNewUser(LoginEntity entity);

    public LoginEntity authenticte(String userName, String password);

    public LoginEntity searchByUserName(String userName);

    public void deleteUser(LoginEntity entity);
}
