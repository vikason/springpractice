/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dao;

import com.entity.LoginEntity;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

/**
 *
 * @author vikas
 */
@Controller("LoginDao")
public class LoginDaoImpl extends BaseDao implements LoginDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public boolean authentication(String userName, String password) {
        return true;
    }

    public LoginDaoImpl() {
    }

    /**
     *
     * @return
     */
    @Override
    public List<LoginEntity> getUserList() {
        Query q = getSession().createQuery("select l from LoginEntity l");
//        q.setString("user", "vikas");

        return q.list();

    }

    @Override
    public void addNewUser(LoginEntity entity) {
        Session s;
        Transaction t;
        s = getSessionFactory().openSession();
        t = s.getTransaction();
        t.begin();
        System.out.println(entity.toString());
        s.saveOrUpdate(entity);
        s.flush();
        t.commit();

    }

    @Override
    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public LoginEntity authenticte(String userName, String password) {
        LoginEntity entity;

        Query q = getSessionFactory().openSession().createQuery(
                "select l from "
                + "LoginEntity l where l.username = :user "
                + "and l.password = :password "
                + "and valid = 'Y'");

        q.setString("user", userName);
        q.setString("password", password);

        entity = (LoginEntity) q.uniqueResult();

        getSessionFactory().openSession().flush();
        return entity;
    }

    @Override
    public void deleteUser(LoginEntity entity) {
        Session session = getSessionFactory().openSession();
        Transaction tr = session.getTransaction();

        tr.begin();
        session.delete(entity);

        session.flush();
        tr.commit();
    }

    @Override
    public LoginEntity searchByUserName(String userName) {
        Session session = getSessionFactory().openSession();
        LoginEntity entity;
        Query q = session.createQuery(
                "select l from "
                + "LoginEntity l where l.username = :user "
        );

        q.setString("user", userName);
        entity = (LoginEntity) q.uniqueResult();
        session.flush();
        session.clear();
        session.close();
        return entity;
    }

}
