package com.dao;

import com.entity.StudentDtl;
import com.entity.StudentMst;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author vikas
 */
@Component("StudentDao")
public class StudentDaoImpl extends BaseDao implements StudentDao {

    @Autowired
    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    protected SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    @Override
    public void saveStudentDetails(
            long rollNO, String name, String address) {

        StudentMst mst = new StudentMst();
        StudentDtl dtl = new StudentDtl();

        mst.setRollNo(rollNO);
        mst.setName(name);
        dtl.setRollNO(rollNO);
        dtl.setAddress(address);
        dtl.setStudentMst(mst);
        mst.setTxn(dtl);
        try {

            session = sessionFactory.openSession();
            transaction = session.getTransaction();
            transaction.begin();
            session.save(dtl.getStudentMst());
            session.save(dtl);
            transaction.commit();

        } finally {
            session.close();
        }

    }
}
