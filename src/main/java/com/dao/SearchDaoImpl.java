/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dao;

import java.util.List;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author vikas
 */
@Component("SearchDao")
public class SearchDaoImpl extends BaseDao implements SearchDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    protected SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public SearchDaoImpl() {

    }

    @Override
    public List printUserDetails(final String userName) {

        Query q = sessionFactory.openSession().createQuery(
                "select new com.entity.LoginUserDetail(e.username, e.password, l.city) "
                + "from com.entity.LoginEntity e, com.entity.UserDetail l "
                + "where l.username = e.username and l.username = :user"
        );

        q.setString("user", userName);
        return q.list();

    }

}
