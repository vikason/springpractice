/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.action;

/**
 *
 * @author vikas
 */
public interface MenuAction {

    String LIST_ALL = "1";
    String ADD_USER = "2";
    String SUSPEND = "3";
    String DELETE = "4";
    String CHANGE_PASSWORD = "5";
    String USER_DETAILS="6";
    String ADD_STUDENT = "7";
    final String LOGOUT = "e";

    public void selectMenu();

}
