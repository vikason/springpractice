/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.action;

import com.dao.SearchDao;
import com.entity.LoginUserDetail;
import java.util.List;

/**
 *
 * @author vikas
 */
public class SearchActionImpl extends BaseAction implements SearchAction {

    private SearchDao dao;

    @Override
    public void displayUserDetails() {
        dao = (SearchDao) getBean("SearchDao");

        List list = dao.printUserDetails("vikas");
        for (Object obj : list) {
            LoginUserDetail detail = (LoginUserDetail) obj;
            System.out.println(detail.toString());
        }
    }
}
