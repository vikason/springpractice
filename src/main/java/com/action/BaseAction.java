/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.action;

import java.io.IOException;
import java.util.Properties;
import java.util.Scanner;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import vik.prop.PropertyUtil;

/**
 *
 * @author vikas
 */
public abstract class BaseAction implements Constants {

    private ApplicationContext ac;
    protected Properties projectConfig;
    protected Scanner s = new Scanner(System.in);

    public BaseAction() {
        try {
            PropertyUtil propertyUtil = new PropertyUtil();

            projectConfig = propertyUtil.getProperties(PROJECT_CONFIG);
            this.ac = new ClassPathXmlApplicationContext(
                    projectConfig.getProperty(SPEING_CONFIG_FILE));

        } catch (IOException ex) {
            System.out.println(ex.getLocalizedMessage());
        }
    }

    public Object getBean(String beanID) {
        return ac.getBean(beanID);
    }

}
