/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.action;

import com.dao.LoginDao;
import com.dao.StudentDao;
import com.entity.LoginEntity;
import java.util.List;

/**
 *
 * @author vikas
 */
public class MenuActionImpl extends BaseAction implements MenuAction {

    private LoginEntity entity;

    private LoginDao loginDao;

    public MenuActionImpl(LoginEntity entity) {
        this.entity = entity;
        loginDao = (LoginDao) getBean("LoginDao");
    }

    @Override
    public void selectMenu() {
        String choice = null;
        while (true) {
            if (choice != null && choice.equalsIgnoreCase(LOGOUT)) {
                break;
            }
            choice = showMenu();
            switch (choice) {

                case LIST_ALL:
                    doListAll();
                    break;
                case ADD_USER:
                    doAddUser();
                    break;
                case SUSPEND:
                    doSuspendUser();
                    break;
                case DELETE:
                    doDeleteUser();
                    break;
                case CHANGE_PASSWORD:
                    doChangePassword();
                    break;
                case USER_DETAILS:
                    getUserDetails();
                case ADD_STUDENT:
                    doAddStudent();
                case LOGOUT:
                    break;
                default:
                    System.out.println("Invalid options selected...");
                    break;
            }
        }
    }

    private String showMenu() {
        System.out.println("Welcome " + entity.getUsername());
        System.out.println("");
        System.out.println("1. List all Users");
        System.out.println("2. Add Users");
        System.out.println("3. Suspend User");
        System.out.println("4. Delect Users");
        System.out.println("5. Change Password");
        System.out.println("6. User Details");
        System.out.println(ADD_STUDENT + ". Add Studetn");

        System.out.println("e. Logout");
        String choice = s.nextLine();
        return choice;
    }

    private void doListAll() {

        List<LoginEntity> list = loginDao.getUserList();

        System.out.println("List of all students--- ");
        for (LoginEntity entity : list) {
            System.out.println(entity.toString());
        }

    }

    private void doAddUser() {
        System.out.println("New User Details--");
        LoginEntity entity = askUserDetail();

        loginDao.addNewUser(entity);

        System.out.println("New User added...");
    }

    private void doDeleteUser() {

        System.out.println("Delete User Details--");
        System.out.println("");
        System.out.println("UserName: ");
        LoginEntity entity;
        entity = loginDao.searchByUserName(s.nextLine());

        System.out.println(entity.toString());
        System.out.println("Are you sure you want to delete (Y): ");
        if (s.nextLine().equalsIgnoreCase("y")) {
            loginDao.deleteUser(entity);
        }

        System.out.println("User Deleted ...");
    }

    private void doSuspendUser() {
        System.out.println("Suspended user...");

        System.out.println("");
        System.out.println("User: ");
        String choice = s.nextLine();
        LoginEntity e = loginDao.searchByUserName(choice);
        e.setValid("S");
        loginDao.addNewUser(e);

    }

    private void doChangePassword() {
        System.out.println("Edit Details ...");
        System.out.println("");
        System.out.println("User: ");
        String choice = s.nextLine();
        LoginEntity e = loginDao.searchByUserName(choice);
        System.out.println(e.toString());

        System.out.println("New password: ");
        e.setPassword(s.nextLine());

        loginDao.addNewUser(e);

    }

    public LoginDao getDao() {
        return loginDao;
    }

    public void setDao(LoginDao dao) {
        this.loginDao = dao;
        selectMenu();
    }

    private LoginEntity askUserDetail() {

        LoginEntity entity = new LoginEntity();
        System.out.println("");
        System.out.println("UserName: ");
        entity.setUsername(s.nextLine());
        System.out.println("password: ");
        entity.setPassword(s.nextLine());
        entity.setValid("Y");

        return entity;
    }

    private void getUserDetails() {
        SearchAction action = new SearchActionImpl();
        action.displayUserDetails();
    }

    private void doAddStudent() {
        System.out.println("Add Student Details ...");
        System.out.println("");
        System.out.println("Roll No: ");
        Long rollNo = Long.parseLong(s.nextLine());
        
        System.out.println("Name: ");
        String name = s.nextLine();
        
        System.out.println("Address: ");
        String add = s.nextLine();
        
        StudentDao dao = (StudentDao)getBean("StudentDao");

        dao.saveStudentDetails(rollNo, name, add);
        
    }

}
