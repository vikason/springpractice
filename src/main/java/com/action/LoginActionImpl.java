package com.action;

import com.dao.LoginDao;
import com.entity.LoginEntity;

/**
 *
 * @author vikas
 */
public class LoginActionImpl extends BaseAction implements LoginAction {

    private LoginDao dao;

    public LoginDao getDao() {
        return dao;
    }

    public void setDao(LoginDao dao) {
        this.dao = dao;
    }

    @Override
    public void doLogin() {

        String userName;
        String password;

        System.out.println("Enter User name: ");
        userName = s.nextLine();

        System.out.println("Enter password : ");
        password = s.nextLine();

        dao = (LoginDao) getBean("LoginDao");

        LoginEntity entity = dao.authenticte(userName, password);
        // authenticate user
        if (entity != null) {

            System.out.println("Authenticated!!!");
            gotoMenu(entity);
         
        } else {
            System.out.println("Don't hack me ...");
        }
//        
//     
//        if (dao.authentication(userName, password)) {
//            System.out.println("Welcome: " + userName);
//
//        } else {
//            System.out.println("Don't try to hack me!!!");
//        }
    }

    private void gotoMenu(LoginEntity entity) {
        MenuAction action = new MenuActionImpl(entity);
        action.selectMenu();
    }

}
