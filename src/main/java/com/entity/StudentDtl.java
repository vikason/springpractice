/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import org.hibernate.annotations.Cascade;

/**
 *
 * @author vikas
 */
@Entity
@Table(name = "student_dtl", catalog = "jsr", schema = "")
@NamedQueries({
    @NamedQuery(name = "StudentDtl.findAll", query = "SELECT s FROM StudentDtl s"),
    @NamedQuery(name = "StudentDtl.findByRollNO", query = "SELECT s FROM StudentDtl s WHERE s.rollNO = :rollNO"),
    @NamedQuery(name = "StudentDtl.findByAddress", query = "SELECT s FROM StudentDtl s WHERE s.address = :address")})
public class StudentDtl implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "rollNO")
    private Long rollNO;
    @Column(name = "address")
    private String address;

    @OneToOne(mappedBy = "txn")
    @Cascade(value = org.hibernate.annotations.CascadeType.ALL)
    private StudentMst studentMst;

    public StudentMst getStudentMst() {
        return studentMst;
    }

    public void setStudentMst(StudentMst studentMst) {
        this.studentMst = studentMst;
    }

    public StudentDtl() {
    }

    public StudentDtl(Long rollNO) {
        this.rollNO = rollNO;
    }

    public Long getRollNO() {
        return rollNO;
    }

    public void setRollNO(Long rollNO) {
        this.rollNO = rollNO;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (rollNO != null ? rollNO.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof StudentDtl)) {
            return false;
        }
        StudentDtl other = (StudentDtl) object;
        if ((this.rollNO == null && other.rollNO != null) || (this.rollNO != null && !this.rollNO.equals(other.rollNO))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.entity.StudentDtl[ rollNO=" + rollNO + " ]";
    }

}
