/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

/**
 *
 * @author vikas
 */
@Entity
@Table(name = "student_mst", catalog = "jsr", schema = "")
@NamedQueries({
    @NamedQuery(name = "StudentMst.findAll", query = "SELECT s FROM StudentMst s"),
    @NamedQuery(name = "StudentMst.findByRollNo", query = "SELECT s FROM StudentMst s WHERE s.rollNo = :rollNo"),
    @NamedQuery(name = "StudentMst.findByName", query = "SELECT s FROM StudentMst s WHERE s.name = :name")})
public class StudentMst implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "rollNo")
    @GeneratedValue(generator = "gen")
    @GenericGenerator(name = "gen", strategy = "foreign", parameters = {
        @Parameter(name = "property", value = "txn")})
    private Long rollNo;
    @Column(name = "name")
    private String name;
    @OneToOne
    @PrimaryKeyJoinColumn
    private StudentDtl txn;

    public StudentDtl getTxn() {
        return txn;
    }

    public void setTxn(StudentDtl txn) {
        this.txn = txn;
    }

    public StudentMst() {
    }

    public StudentMst(Long rollNo) {
        this.rollNo = rollNo;
    }

    public Long getRollNo() {
        return rollNo;
    }

    public void setRollNo(Long rollNo) {
        this.rollNo = rollNo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (rollNo != null ? rollNo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof StudentMst)) {
            return false;
        }
        StudentMst other = (StudentMst) object;
        if ((this.rollNo == null && other.rollNo != null) || (this.rollNo != null && !this.rollNo.equals(other.rollNo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.entity.StudentMst[ rollNo=" + rollNo + " ]";
    }

}
