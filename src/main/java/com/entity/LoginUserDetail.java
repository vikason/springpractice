/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.entity;

/**
 *
 * @author vikas
 */
public class LoginUserDetail {

    private String username;
    private String password;
    private String city;

    public LoginUserDetail(String username, String password, String city) {
        this.username = username;
        this.password = password;
        this.city = city;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Override
    public String toString() {
        return "LoginUserDetail{" + "username=" + username + ", password=" + password + ", city=" + city + '}';
    }

}
